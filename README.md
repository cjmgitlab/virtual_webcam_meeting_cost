# virtual_webcam_meeting_cost

## Purpose

This Python script creates a virtual webcam that shows "This meeting has COST" and then a dollar value in big red font.  I created this virtual webcam to remind myself and other meeting participants that time is money and sometimes not everyone is required at meetings (how many of us have been in a meeting with 37 people and only 3 or 4 people are actively engaged?).

## Prerequisites:

- [ ] Install OBS Studio.  Note: I first tried a portable version of OBS Studio but it didn't provide the virtual webcam feature, so I had to install the full application.
- [ ] Install Python
- [ ] Install the required Python modules using pip:

```
pip install opencv-python numpy pyvirtualcam
```

## How to Run the Script

- [ ] Open a Command Prompt and tell Python to run the script

```
python virtual_webcam_meeting_cost.py
```

- [ ] Open OBS Studio to view the webcam to verify it is working.
- [ ] Close OBS Studio and open your collaboration tool (Google Meet, Zoom, Skype, Microsoft Teams, etc.) and select the virtual webcam

## Notes

- I've only test this on Windows.  Presumably it would work on other operating systems that can run OBS Studio

## Authors and acknowledgment
- Maik Riechert for pyvirtualcam (https://github.com/letmaik/pyvirtualcam)
- ChatGPT for creating the first proof-of-concept version for me (which displayed the time in the virtual webcam)

## License
Apache 2.0 license (https://www.apache.org/licenses/LICENSE-2.0)

## Project status
Did this for fun during the holidays and might update it occasionally once work resumes or if I get feedback from coworkers/friends on how to improve it.
