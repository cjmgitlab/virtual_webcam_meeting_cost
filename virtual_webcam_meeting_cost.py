# Meeting Cost Virtual Webcam
# 
# Description:  Creates a virtual webcam (using OBS Studio) that displays the cost of the meeting
#               by taking inputs of average fully burdened labor cost per hour and number of
#               meeting participants.  Cost updates every minute.
#
# History
# Date          Contributor         Change
# 12/29/2023    Chris Meagher       Initial version with help from ChatGPT
# 01/05/2024    Chris Meagher       Revised to update the webcam display whenever user requests update
#
# Note on dependencies:
# Site Packages Needed:
# > opencv-python (for cv2)
# > numpy
# > pyvirtualcam
#
# Known Issues/TODO:
# - webcam only refreshes every 60 seconds (WEBCAM_REFRESH_TIME), so the GUI will show more up-to-date meeting cost value until the webcam refreshes
# - no way currently to refresh the webcam display on press of Update button
#
import cv2                          # "computer vision" used to generate the frame for the virtual webcam
import numpy as np                  # Used to create an array of zeros
import pyvirtualcam                 # Used to display the virtual camera
from datetime import datetime       # Used to save the start time and display the update times
import threading                    # Needed to create separate threads for the GUI vs the virtual webcam loop
import time                         # Used for time.sleep() commands
import tkinter as tk                # Used to draw the GUI
from tkinter import messagebox      # Used for on-close message box

# Constants
FRAME_PADDING = 0
NUM_ROWS = 5
NUM_COLS = 3
SMALL_FONT = cv2.FONT_HERSHEY_SIMPLEX
SMALL_FONT_SCALE = 2
SMALL_FONT_COLOR = (255, 255, 255)
SMALL_FONT_THICKNESS = 2
LARGE_FONT = cv2.FONT_HERSHEY_SIMPLEX
LARGE_FONT_SCALE = 6
LARGE_FONT_COLOR = (255, 0, 0)
LARGE_FONT_THICKNESS = 20
LINE1_ORIGIN = (50, 110)
LINE2_ORIGIN = (250, 180)
LINE3_ORIGIN = (20, 360)
WEBCAM_REFRESH_TIME = 60 # seconds

# Global Variables
calculated_cost = 0
update_flag_set = False

# Mutual Exclusion (mutex) locks for the threads that will be changing or reading global variables
lock_cost = threading.Lock()
lock_update_flag = threading.Lock()

# Grab the start time
start_time = datetime.now()

def generate_frame():
    # Create a black image
    frame = np.zeros((480, 640, 3), dtype=np.uint8)

    with lock_cost:
        cost_to_display = calculated_cost
        #debug# print("Would have displayed this value: " + str(calculated_cost))
    
    cv2.putText(frame, "This meeting has", LINE1_ORIGIN, SMALL_FONT, SMALL_FONT_SCALE, SMALL_FONT_COLOR, SMALL_FONT_THICKNESS, cv2.LINE_AA)
    cv2.putText(frame, "COST", LINE2_ORIGIN, SMALL_FONT, SMALL_FONT_SCALE, SMALL_FONT_COLOR, SMALL_FONT_THICKNESS, cv2.LINE_AA)
    cv2.putText(frame, "$" + str(cost_to_display), LINE3_ORIGIN, LARGE_FONT, LARGE_FONT_SCALE, LARGE_FONT_COLOR, LARGE_FONT_THICKNESS, cv2.LINE_AA)

    return frame

def webcam_main():
    global update_flag_set
    
    # Initialize virtual camera
    with pyvirtualcam.Camera(width=640, height=480, fps=20) as cam:
        print(f'Using virtual camera: {cam.device}')

        # Loop forever and send updated camera frames whenever requested
        while True:
            frame = generate_frame()
            cam.send(frame)

            with lock_update_flag:
                if update_flag_set:
                    frame = generate_frame()
                    cam.send(frame)
                    update_flag_set = False

            time.sleep(1)
        
#------------------------------------------------------------------------------
# Main Program
#------------------------------------------------------------------------------
if __name__ == '__main__':
    # Run the virtual webcam as a thread since it needs to be on its own forever while loop
    webcam_thread = threading.Thread(target=webcam_main)
    webcam_thread.daemon = True
    webcam_thread.start()

    # Run the GUI.  Can't run it as a thread since tkinter has known issue with "Tcl_AsyncDelete: async handler deleted by the wrong thread" when closing it as a thread
    # - periodically updates the calculated_cost global variable based on time and user input
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # update_calculations()
    def update_calculations():
        global calculated_cost
        global start_time
        
        current_time = datetime.now()        
        minute_rate = float(hourly_rate.get()) / 60.0
        participants = float(num_participants.get())
        total_minute_rate = minute_rate * participants
        num_minutes = float(((current_time - start_time).seconds) / 60.0)

        with lock_cost:
            calculated_cost = round(minute_rate * participants * num_minutes)
        
        with lock_update_flag:
            update_flag_set = True

        last_checked_value.config(text=current_time.strftime("%Y-%m-%d %H:%M:%S"))
        calculated_minute_rate_value.configure(text="= $" + str(round(minute_rate, 2)) + "/min")
        calculated_total_minute_rate_value.configure(text="$" + str(round(total_minute_rate, 2)) + "/min")
        calculated_num_minutes_value.configure(text=str(round(num_minutes)))
        calculated_cost_value.configure(text="$" + str(calculated_cost))
        
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # periodic_call()
    # - Gets called initially after 1 second and then repeatedly called every WEBCAM_REFRESH_TIME seconds
    def periodic_call():
        update_calculations()
        root.after(WEBCAM_REFRESH_TIME * 1000, periodic_call)
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # on_closing()
    # - Gets called when the user tries to close the GUI, giving them an option to cancel the close
    def on_closing():
        if messagebox.askokcancel("Quit", "Do you want to quit?"):
            root.destroy()

    # Define the tkinter GUI properties
    root = tk.Tk()
    root.title("Meeting Cost Virtual Webcam v1.0")
    root.geometry('450x120')

    for i in range(NUM_ROWS):
        root.rowconfigure(i, weight=1, minsize=0)
    for i in range(NUM_COLS):
        root.columnconfigure(i, weight=1, minsize=0)

    # First Row: Labor Costs
    frame = tk.Frame(master=root)
    frame.grid(row=0, column=0)
    label = tk.Label(master=frame, text="Burdened Labor Cost per Hour")
    label.pack(padx=FRAME_PADDING, pady=FRAME_PADDING)

    frame = tk.Frame(master=root)
    frame.grid(row=0, column=1)
    hourly_rate = tk.Entry(master=frame)
    hourly_rate.insert(0, "150")
    hourly_rate.pack(padx=FRAME_PADDING, pady=FRAME_PADDING)

    # Second Row: Number of Participants
    frame = tk.Frame(master=root)
    frame.grid(row=1, column=0)
    label = tk.Label(master=frame, text="Number of People in Meeting:")
    label.pack(padx=FRAME_PADDING, pady=FRAME_PADDING)

    frame = tk.Frame(master=root)
    frame.grid(row=1, column=1)
    num_participants = tk.Entry(master=frame)
    num_participants.insert(0, "1")
    num_participants.pack(padx=FRAME_PADDING, pady=FRAME_PADDING)

    frame = tk.Frame(master=root)
    frame.grid(row=0, column=2)
    calculated_minute_rate_value = tk.Label(master=frame, text="")
    calculated_minute_rate_value.pack(padx=FRAME_PADDING, pady=FRAME_PADDING)
    
    # Third Row: Update Button and time of last update
    # Update button
    frame = tk.Frame(master=root)
    frame.grid(row=2, column=0)
    update_button = tk.Button(frame, text="Update", command=update_calculations)
    update_button.grid(row=2, column=0, sticky = tk.W+tk.E)
    update_button.pack(padx=FRAME_PADDING, pady=FRAME_PADDING)

    frame = tk.Frame(master=root)
    frame.grid(row=2, column=1)
    label = tk.Label(master=frame, text="Last Updated")
    label.pack(padx=FRAME_PADDING, pady=FRAME_PADDING)

    frame = tk.Frame(master=root)
    frame.grid(row=2, column=2)
    last_checked_value = tk.Label(master=frame, text="")
    last_checked_value.pack(padx=FRAME_PADDING, pady=FRAME_PADDING)

    # Fourth Row: Labels for calculated values
    frame = tk.Frame(master=root)
    frame.grid(row=3, column=0)
    label = tk.Label(master=frame, text="Total Cost per Minute")
    label.pack(padx=FRAME_PADDING, pady=FRAME_PADDING)

    frame = tk.Frame(master=root)
    frame.grid(row=3, column=1)
    label = tk.Label(master=frame, text="Minutes Since Meeting Start")
    label.pack(padx=FRAME_PADDING, pady=FRAME_PADDING)

    frame = tk.Frame(master=root)
    frame.grid(row=3, column=2)
    label = tk.Label(master=frame, text="Total Cost")
    label.pack(padx=FRAME_PADDING, pady=FRAME_PADDING)

    # Fifth Row: Calculated values (to double-check what should be displayed on webcam)
    frame = tk.Frame(master=root)
    frame.grid(row=4, column=0)
    calculated_total_minute_rate_value = tk.Label(master=frame, text="")
    calculated_total_minute_rate_value.pack(padx=FRAME_PADDING, pady=FRAME_PADDING)

    frame = tk.Frame(master=root)
    frame.grid(row=4, column=1)
    calculated_num_minutes_value = tk.Label(master=frame, text="")
    calculated_num_minutes_value.pack(padx=FRAME_PADDING, pady=FRAME_PADDING)

    frame = tk.Frame(master=root)
    frame.grid(row=4, column=2)
    calculated_cost_value = tk.Label(master=frame, text="")
    calculated_cost_value.pack(padx=FRAME_PADDING, pady=FRAME_PADDING)
    
    # Call periodic check function for the first time, after 30000ms (30sec) since it may take a couple runs of check_firewall (run every 15sec) to get valid results 
    root.after(1000, periodic_call)

    # Enable to message box asking to confirm when the user tries to close the window
    root.protocol("WM_DELETE_WINDOW", on_closing)

    # Main loop is used to run the main tkinter application
    root.mainloop()
